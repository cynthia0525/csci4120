﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    const int MAX_HEALTH = 100;
    const int MIN_HEALTH = 0;

    int healthPoint = MAX_HEALTH;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        GameObject target = hit.gameObject;
        if (target.tag == "Props") target.GetComponent<Props>().OnPlayerCollide(this);
    }

    public int GetHealth()
    {
        return healthPoint;
    }

    public void ChangeHealth(int amount)
    {
        healthPoint += amount;
        healthPoint = healthPoint > MAX_HEALTH ? MAX_HEALTH : healthPoint < MIN_HEALTH ? MIN_HEALTH : healthPoint;
        HealthBar.instance.UpdateHealth((float)healthPoint / MAX_HEALTH);

        if (healthPoint == 0) Timer.instance.EndGame(false);
    }
}
