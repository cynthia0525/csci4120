﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakWall : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        int score = Score.instance.GetScore();

        if (other.tag == "Player" && score > 360)
            Destroy(gameObject);
    }
}
