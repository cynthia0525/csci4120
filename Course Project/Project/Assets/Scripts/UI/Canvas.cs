﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas : MonoBehaviour
{
    public void NextLevel()
    {
        BackgroundMusic.instance.NextLevel();
    }

    public void ChangeScene(string nextScene)
    {
        BackgroundMusic.instance.ChangeScene(nextScene);
    }
}
