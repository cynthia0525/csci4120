﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public static HealthBar instance { get; private set; }

    Image mask;
    float originalSize;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        mask = GetComponent<Image>();
        originalSize = mask.rectTransform.rect.width;
    }

    public void UpdateHealth(float value)
    {
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }
}
