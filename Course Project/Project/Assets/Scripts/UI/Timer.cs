﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public static Timer instance { get; private set; }

    Character player;
    Text text;
    int timeLeft;

    [Header("[Setting]")]
    [SerializeField]
    int timeLimit = 0;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Character>();
        text = GetComponent<Text>();
        timeLeft = timeLimit;
        InvokeRepeating("Countdown", 0f, 1f);
        InvokeRepeating("ReduceHealth", 0f, 5f);

        // unlimited time for tutorial
        if (SceneManager.GetActiveScene().name == "Tutorial")
            Invoke("StopCountdown", 30f);
    }

    void Countdown()
    {
        UpdateTimer(-1);

        if (timeLeft == 0) EndGame(false);
    }

    void ReduceHealth()
    {
        player.ChangeHealth(-1);
    }

    void StopCountdown()
    {
        CancelInvoke("Countdown");
        CancelInvoke("ReduceHealth");
    }

    public void EndGame(bool isClear)
    {
        StopCountdown();
        if (isClear)
        {
            int bonus = timeLeft * 10 + player.GetHealth();
            Score.instance.UpdateScore(bonus);
        }
        BackgroundMusic.instance.ChangeScene(isClear ? "GameClear" : "GameOver");
    }

    public void UpdateTimer(int amount)
    {
        timeLeft += amount;
        text.text = timeLeft.ToString();
    }
}
