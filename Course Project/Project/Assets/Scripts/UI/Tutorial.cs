﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    GameObject guide = null;

    void Awake()
    {
        guide.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") guide.SetActive(true);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") guide.SetActive(false);
    }
}
