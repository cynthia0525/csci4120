﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    [SerializeField]
    bool isRecordBoard = false;

    void Start()
    {
        if (isRecordBoard) DisplayRecordBoard();
        else DisplayScoreBoard();
    }

    void DisplayRecordBoard()
    {
        foreach (string level in BackgroundMusic.instance.LEVELS)
        {
            Text text = transform.Find(level + "/Score").GetComponent<Text>();
            int score = PlayerPrefs.GetInt(level, 0);
            text.text = score.ToString();
        }
    }

    void DisplayScoreBoard()
    {
        string level = PlayerPrefs.GetString("Game Level", "Error");

        Text currentScore = transform.Find("Score/Score").GetComponent<Text>();
        int score = PlayerPrefs.GetInt("Score", 0);
        currentScore.text = score.ToString();

        Text bestScore = transform.Find("Best/Score").GetComponent<Text>();
        int best = PlayerPrefs.GetInt(level, 0);
        bestScore.text = best.ToString();

        if (score > best) PlayerPrefs.SetInt(level, score);
    }
}
