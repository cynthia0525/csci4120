﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    public static Score instance { get; private set; }

    Text text;
    int score;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        text = GetComponent<Text>();
        score = 0;
        UpdateScore(0);
    }

    void OnDestroy()
    {
        string level = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("Game Level", level);
        PlayerPrefs.SetInt("Score", score);
    }

    public int GetScore()
    {
        return score;
    }

    public void UpdateScore(int amount)
    {
        score += amount;
        text.text = score.ToString();
    }
}
