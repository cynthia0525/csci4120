﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    Animator anim;
    NavMeshAgent agent;
    Character player;
    bool chaseMode = false;
    bool isCollided = false;

    [Header("[Setting]")]
    [SerializeField]
    float chasingDistance = 0;
    [SerializeField]
    int demage = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.transform.position - transform.position;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, (direction + Vector3.up).normalized, out hit, chasingDistance))
            chaseMode = hit.transform.tag == "Player" && Mathf.Abs(Vector3.Angle(transform.forward, direction)) <= 80;
        else chaseMode = false;

        anim.SetBool("chaseMode", chaseMode);
        agent.enabled = chaseMode;
        if (chaseMode)
        {
            transform.LookAt(player.transform.position);
            agent.SetDestination(player.transform.position);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (isCollided) return;
            isCollided = true;
            player.ChangeHealth(-demage);
            Invoke("DetectCollision", 5);
        }
    }

    void DetectCollision()
    {
        isCollided = false;
    }
}
