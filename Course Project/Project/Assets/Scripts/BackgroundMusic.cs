﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundMusic : MonoBehaviour
{
    public static BackgroundMusic instance { get; private set; }
    public readonly List<string> LEVELS = new List<string> { "Tutorial", "Level1", "Level2", "Level3" };
    AudioSource audioSource;

    [Header("[Background Music]")]
    [SerializeField]
    AudioClip mainBackgoundMusic = null, gameBackgoundMusic = null;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);

        audioSource = GetComponent<AudioSource>();
        ChangeScene("LandingScene");
    }

    public void NextLevel()
    {
        string currentLevel = PlayerPrefs.GetString("Game Level", "Error");
        int levelIndex = LEVELS.IndexOf(currentLevel) + 1;
        string nextScene = LEVELS.Count == levelIndex ? "LevelScene" : LEVELS[levelIndex];
        ChangeScene(nextScene);
    }

    public void ChangeScene(string nextScene)
    {
        SceneManager.LoadScene(nextScene);
        audioSource.clip = LEVELS.Contains(nextScene) ? gameBackgoundMusic : mainBackgoundMusic;
        audioSource.Play();
    }
}
