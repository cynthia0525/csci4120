﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Props : MonoBehaviour
{
    bool isCollided = false;

    [Header("[Setting]")]
    [SerializeField]
    bool isHealth = false;
    [SerializeField]
    int amount = 0;
    [SerializeField]
    int score = 0;

    public void OnPlayerCollide(Character player)
    {
        if (isCollided) return;
        isCollided = true;
        if (isHealth) player.ChangeHealth(amount);
        else Timer.instance.UpdateTimer(amount);
        Score.instance.UpdateScore(score);
        Destroy(gameObject);
    }
}
