﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallOver : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            Timer.instance.EndGame(false);
    }
}
