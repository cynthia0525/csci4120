# CSCI4120 Principles of Computer Game Software (Spring 2020)
This course aims at establishing the principles, techniques and tools in the design and development of computer game software with focus on the real time performance consideration. Topics include: stages in computer game development, concept of game engine, rendering considerations, physics effects, artificial intelligence (AI), audio effects, scripting and environment for game project development.

# Grading
* 10% Assignment 1 [82/100]
* 10% Assignment 2 [98/100]
* 10% Assignment 3 [100/100]
* 30% Extra Assignment
  * Programming [70/100]
  * Written
* 40% Course Project