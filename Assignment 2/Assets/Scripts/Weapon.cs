﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    int force = 0;
    public int shootForce { get { return force; } }

    [SerializeField]
    int atk = 0;
    public int attackPoint { get { return atk; } }

    Transform target = null;
    public Transform weaponTarget { get { return target; } set { target = value; } }

    void Update()
    {
        if (weaponTarget != null)
        {
            transform.position += transform.forward * shootForce * Time.deltaTime;
            transform.LookAt(target.position + Vector3.up * 0.5f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Weapon") return;
        GetComponent<AudioSource>().Play();
        Destroy(gameObject, 0.25f);
    }
}
