﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Tank
{
    void Start()
    {
        layerMask = LayerMask.GetMask("Enemy");

        // start in front of the goal
        transform.position = goalPoint.position + goalPoint.forward * 2.5f;
        transform.rotation = goalPoint.rotation;
    }

    void Update()
    {
        // move
        float forwardAmount = Input.GetAxis("Vertical") * forwardRate;
        float turnForce = Input.GetAxis("Horizontal") * turnRate;
        Move(forwardAmount, turnForce);

        // fire
        if (Input.GetButtonDown("Fire1")) Attack();
        if (Input.GetButtonDown("Fire2")) SpecialAttack();
    }
}
