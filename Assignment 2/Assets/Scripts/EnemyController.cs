﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Tank
{
    bool patrolMode = false;
    int destPoint = 0;
    bool attackMode = false;
    NavMeshAgent agent;
    GameObject[] waypoints;
    GameObject player;

    public void InitWorkingMode(bool goalSeeking)
    {
        goalPoint = GameObject.FindGameObjectWithTag("Goal").transform;
        agent = GetComponent<NavMeshAgent>();
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        player = GameObject.FindGameObjectWithTag("Player");

        // finite state machine
        patrolMode = !goalSeeking;
        if (patrolMode)
        {
            agent.autoBraking = false;
            PatrolToNextPoint();
        }
        else agent.SetDestination(goalPoint.position);
        StartCoroutine("MoveOrAttack");
    }

    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            if (patrolMode) PatrolToNextPoint();
            else
            {
                agent.isStopped = true;
                StopCoroutine("MoveOrAttack");
            }
        }
    }

    void FixedUpdate()
    {
        Vector3 direction = player.transform.position - transform.position;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, (direction + Vector3.up * 0.5f).normalized, out hit, scanRange))
        {
            bool nextMode = hit.transform.tag == "Player" && Mathf.Abs(Vector3.Angle(transform.forward, direction)) <= 60;
            attackMode = nextMode;
            agent.isStopped = nextMode;
        }
    }

    void PatrolToNextPoint()
    {
        if (waypoints.Length == 0) return;
        agent.SetDestination(waypoints[destPoint].transform.position);
        destPoint = (destPoint + 1) % waypoints.Length;
    }

    IEnumerator MoveOrAttack()
    {
        while (true)
        {
            if (attackMode)
            {
                transform.LookAt(player.transform.position);
                Attack();
                yield return new WaitForSeconds(1);
            }
            else yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
