﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    GameObject enemy = null;
    [SerializeField]
    List<Vector3> initialLocation = null;

    void Start()
    {
        StartCoroutine("SpawnEnemy");
    }

    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            for (int i = 0; i < 2; i++)
            {
                int index = Random.Range(0, initialLocation.Count);
                GameObject spawn = Instantiate(enemy, initialLocation[index], enemy.transform.rotation);
                spawn.GetComponent<EnemyController>().InitWorkingMode(i == 0);
            }
            yield return new WaitForSeconds(10);
        }
    }
}
