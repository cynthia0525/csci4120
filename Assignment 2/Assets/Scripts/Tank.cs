﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    // health point
    const int MAX_HEALTH = 300;
    int hp = MAX_HEALTH;
    public int healthPoint { get { return hp; } set { hp = value; } }

    [SerializeField]
    protected Transform goalPoint = null;
    [SerializeField]
    protected int scanRange = 0;
    protected int layerMask;

    [Header("[Movement Setting]")]
    // movement
    [SerializeField]
    protected float forwardRate = 0;
    [SerializeField]
    protected float turnRate = 0;

    [Header("[Weapon Setting]")]
    [SerializeField]
    Transform shootPosition = null;
    // bullet
    [SerializeField]
    GameObject bulletPrefab = null;
    // missile
    [SerializeField]
    GameObject missilePrefab = null;

    void OnCollisionEnter(Collision collision)
    {
        GameObject collider = collision.gameObject;
        if (collider.tag == "Weapon")
        {
            Weapon weapon = collider.GetComponent<Weapon>();
            healthPoint -= weapon.attackPoint;
            if (healthPoint <= 0) Destroy(gameObject);
        }
    }

    protected void Move(float translateF, float rotateY)
    {
        transform.position += transform.forward * translateF * Time.deltaTime;
        transform.Rotate(0, rotateY, 0);
    }

    GameObject Fire(GameObject weapon, Vector3 offset)
    {
        return Instantiate(weapon, transform.position + offset, shootPosition.rotation);
    }

    protected void Attack()
    {
        GameObject bulletObject = Fire(bulletPrefab, (Vector3.up + transform.forward) * 1.5f);
        bulletObject.GetComponent<Rigidbody>().AddForce(shootPosition.forward * bulletObject.GetComponent<Weapon>().shootForce);
        Destroy(bulletObject, 5f);
    }

    protected void SpecialAttack()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, scanRange, layerMask);
        foreach (Collider collider in colliders)
        {
            GameObject missileObject = Fire(missilePrefab, Vector3.up * 3f);
            missileObject.GetComponent<Weapon>().weaponTarget = collider.transform;
        }
    }
}
