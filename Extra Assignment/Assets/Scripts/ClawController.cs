﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawController : MonoBehaviour
{
    HingeJoint leftGrip, rightGrip;
    Vector3 targetPosition, initialPosition, designatedPosition;
    float initialHeight;

    string mode = null; // null / grab / relax
    bool moveAngle = false;
    bool moveTop = false;
    bool sleep = false;
    [SerializeField]
    float speed;
    [SerializeField]
    float minHeight;

    float offset { get { return speed * Time.deltaTime; } }

    void ToggleSleep()
    {
        sleep = !sleep;
    }

    void WaitForOneSecond()
    {
        ToggleSleep();
        Invoke("ToggleSleep", 1);
    }

    // Start is called before the first frame update
    void Start()
    {
        leftGrip = GameObject.Find("/Claw/Left Claw/Grip").GetComponent<HingeJoint>();
        rightGrip = GameObject.Find("/Claw/Right Claw/Grip").GetComponent<HingeJoint>();
        initialPosition = transform.position;
        designatedPosition = transform.position;
        designatedPosition.x *= -1;
        initialHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (sleep) return;
        bool isGrab = mode == "grab";
        if (moveAngle) MoveAngle(isGrab);
        else if (moveTop) MoveTop(isGrab);
        else if (!string.IsNullOrEmpty(mode))
        {
            if (initialPosition == targetPosition && transform.position == initialPosition)
            {
                if (isGrab) mode = null;
                else
                {
                    mode = "grab";
                    moveAngle = true;
                }
                return;
            }
            OnGrab(isGrab);
        }
        else
        {
            // move
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            transform.position += new Vector3(horizontal * offset, 0, vertical * offset);

            // grab
            if (Input.GetButtonDown("Grab"))
            {
                mode = "relax";
                moveAngle = true;
                moveTop = true;
            }
        }
    }

    void MoveAngle(bool isGrab)
    {
        float targetAngle = isGrab ? 0 : 45;
        JointSpring leftSpring = leftGrip.spring;
        JointSpring rightSpring = rightGrip.spring;
        if (leftSpring.targetPosition == targetAngle && rightSpring.targetPosition == -targetAngle)
        {
            moveAngle = false;
            return;
        }

        // move
        float angle = isGrab ? -1 : +1;
        leftSpring.targetPosition += angle;
        leftGrip.spring = leftSpring;
        rightSpring.targetPosition -= angle;
        rightGrip.spring = rightSpring;
    }

    float MoveDistance(float targetDistance)
    {
        float unitDistance = offset / 2;
        return targetDistance > 0 ? Mathf.Min(targetDistance, unitDistance) : Mathf.Max(targetDistance, unitDistance * -1);
    }

    void MoveTop(bool isGrab)
    {
        float targetTop = isGrab ? initialHeight : minHeight;
        float targetDistance = targetTop - transform.position.y;
        if (targetDistance == 0)
        {
            moveTop = false;
            if (isGrab)
            {
                WaitForOneSecond();
                targetPosition = designatedPosition;
            }
            else
            {
                mode = "grab";
                moveAngle = true;
                moveTop = true;
            }
            return;
        }

        // move
        transform.position += new Vector3(0, MoveDistance(targetDistance), 0);
    }

    void OnGrab(bool isGrab)
    {
        float targetHorizontal = targetPosition.x - transform.position.x;
        float targetVertical = targetPosition.z - transform.position.z;
        if (targetHorizontal == 0 && targetVertical == 0)
        {
            mode = "relax";
            moveAngle = true;
            targetPosition = initialPosition;
            return;
        }

        // move
        transform.position += targetHorizontal == 0 ? new Vector3(0, 0, MoveDistance(targetVertical)) : new Vector3(MoveDistance(targetHorizontal), 0, 0);
    }
}
