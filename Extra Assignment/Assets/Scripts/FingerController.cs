﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerController : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        collision.transform.SetParent(transform.parent);
        collision.rigidbody.isKinematic = true;
        collision.rigidbody.useGravity = false;
    }

    void OnCollisionExit(Collision collision)
    {
        collision.transform.SetParent(null);
        collision.rigidbody.isKinematic = false;
        collision.rigidbody.useGravity = true;
    }
}
