﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator animator;
    Rigidbody rigidBody;
    CapsuleCollider capsuleCollider;

    // jumping variables
    [SerializeField]
    float jumpForce = 500;
    [SerializeField]
    float groundDistance = 0.5f;
    [SerializeField]
    LayerMask groundMask;

    // crouching variables
    float capsuleHeight;
    Vector3 capsuleCenter;
    float positionY;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        capsuleHeight = capsuleCollider.height;
        capsuleCenter = capsuleCollider.center;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        animator.SetFloat("Speed", vertical);
        animator.SetFloat("Turning Speed", horizontal);

        bool isGrounded = animator.GetBool("Is Grounded");
        bool isCrouching = animator.GetBool("Is Crouching");

        if (!isCrouching && Input.GetButtonDown("Jump"))
        {
            rigidBody.AddForce(Vector3.up * jumpForce);
            animator.SetTrigger("Jump");
        }
        else if (isGrounded && Input.GetButtonDown("Crouch"))
        {
            positionY = transform.position.y;
            animator.SetBool("Is Crouching", !isCrouching);
            capsuleCollider.height = isCrouching ? capsuleHeight : capsuleHeight * 0.5f;
            capsuleCollider.center = isCrouching ? capsuleCenter : capsuleCenter * 0.5f;
        }

        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, groundDistance, groundMask))
        {
            animator.SetBool("Is Grounded", true);
            animator.applyRootMotion = true;
        }
        else animator.SetBool("Is Grounded", false);

        if (isGrounded && isCrouching)
        {
            Vector3 transformPosition = transform.position;
            transformPosition.y = positionY;
            transform.position = transformPosition;
        }
    }
}
